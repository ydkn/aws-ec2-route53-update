variable "default_zone_id" {
  type        = string
  description = "Default Zone ID if not set on EC2 instance"
  default     = ""
}

resource aws_iam_role "role" {
  name = "ec2-route53-update"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource aws_iam_role_policy_attachment "AWSOpsWorksCloudWatchLogs" {
  role       = "${aws_iam_role.role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AWSOpsWorksCloudWatchLogs"
}

resource aws_iam_role_policy "policy" {
  name = "ec2-route53-update"
  role = "${aws_iam_role.role.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ec2:DescribeInstances"
      ],
      "Effect": "Allow",
      "Resource": "*"
    },
    {
      "Action": [
        "route53:ChangeResourceRecordSets"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource aws_lambda_function "function" {
  function_name = "ec2-route53-update"
  description   = "Update Route53 record sets on EC2 instance state changes"

  role = "${aws_iam_role.role.arn}"

  s3_bucket = "aws-ec2-route53-update"
  s3_key    = "lambda_function.zip"

  runtime = "python3.7"
  handler = "lambda_function.lambda_handler"

  memory_size = 128
  timeout     = 10

  environment {
    variables = {
      ROUTE53_ZONEID = "${var.default_zone_id}"
    }
  }
}

resource aws_cloudwatch_event_rule "pattern" {
  name        = "ec2-route53-update"
  description = "Update Route53 record sets on EC2 instance state changes"

  is_enabled = true

  event_pattern = <<EOF
{
  "source": [
    "aws.ec2"
  ],
  "detail-type": [
    "EC2 Instance State-change Notification"
  ],
  "detail": {
    "state": ["running", "stopped"]
  }
}
EOF
}

resource aws_cloudwatch_event_target "target" {
  rule = "${aws_cloudwatch_event_rule.pattern.name}"
  arn  = "${aws_lambda_function.function.arn}"
}

resource aws_lambda_permission "permission" {
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.function.function_name}"
  principal     = "events.amazonaws.com"
  source_arn    = "${aws_cloudwatch_event_rule.pattern.arn}"
}
