# aws-ec2-route53-update

Update Route53 record sets with public IP addresses of an EC2 instance on state change.  
Please see [my blog post](https://ydkn.io/update-resource-record-in-route53-hosted-zone-on-ec2-instance-boot-2/) about this project.


## Installation

### CloudFormation

Use the following S3 URL when creating the Cloudformation stack:  
https://aws-ec2-route53-update.s3.eu-central-1.amazonaws.com/cloudformation.yml

You can set a default Route53 zone ID as a parameter for the stack or leave it empty if you don't want to set a default zone.

You can also use this CloudFormation Quick-Create link to get started: [Click Here](https://eu-central-1.console.aws.amazon.com/cloudformation/home?region=eu-central-1#/stacks/create/review?templateURL=https://aws-ec2-route53-update.s3.eu-central-1.amazonaws.com/cloudformation.yml&stackName=ec2-route53-update)

### Terraform

Use it as a module:

```hcl
provider aws {
  region     = "eu-central-1"
  access_key = "XXX"
  secret_key = "XXX"
}

module "aws-ec2-route53-update" {
  source = "git::https://gitlab.com/ydkn/aws-ec2-route53-update.git"

  default_zone_id = "FOOBAR"
}
```

The _default_zone_id_ variable is optional.


## Usage

- Tag your EC2 instances with the key **ROUTE53_ZONEID** to specify the Route53 zone ID in which the hostname will be managed. If you do not specify this tag the Cloudformation/Terraform parameter will be used.
- Tag your EC2 instances with the key **ROUTE53_HOSTNAME** to specify the FQDN used within the Route53 zone.


## License

[MIT License](http://opensource.org/licenses/MIT)
