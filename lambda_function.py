import os
import boto3

# DNS record values used for stopped instances
STOPPED_IPV4 = '192.0.2.1' # RFC5737
STOPPED_IPV6 = '2001:db8::1' # RFC3849

def lambda_handler(event, context):
    if event['source'] != 'aws.ec2':
        print('Non EC2 event')
        return

    instance_id = event['detail']['instance-id']
    instance_details = describe_instance(instance_id)
    zone_id = route53_zone_id(instance_details)
    hostname = route53_hostname(instance_details)
    ipv4 = instance_ipv4(instance_details)
    ipv6 = instance_ipv6(instance_details)

    # Fallback to environment variable for zone id if available
    if (zone_id == None or zone_id == '') and os.environ['ROUTE53_ZONEID'] != '':
        zone_id = os.environ['ROUTE53_ZONEID']

    if zone_id == None or zone_id == '':
        print("Tag 'ROUTE53_ZONEID' not set on instance " + instance_id)
        return

    if hostname == None or hostname == '':
        print("Tag 'ROUTE53_HOSTNAME' not set on instance " + instance_id)
        return

    if event['detail']['state'] == 'running':
        change_recordsets(zone_id, hostname, ipv4, ipv6)
    elif event['detail']['state'] == 'stopped':
        change_recordsets(zone_id, hostname, STOPPED_IPV4, STOPPED_IPV6)

    print('Updated records for hostname ' + hostname + ' to instance ' + instance_id)

def tag_value(details, tag_key):
    for tag in details['Tags']:
        if tag['Key'] == tag_key:
            return tag['Value']

# Get EC2 instance details
def describe_instance(instance_id):
    client = boto3.client('ec2')
    resp = client.describe_instances(InstanceIds=[instance_id])
    return resp['Reservations'][0]['Instances'][0]

# Get the Route53 zone id from instance tags
def route53_zone_id(instance_details):
    return tag_value(instance_details, 'ROUTE53_ZONEID')

# Get the Route53 hostname from instance tags
def route53_hostname(instance_details):
    return tag_value(instance_details, 'ROUTE53_HOSTNAME')

# Get the IPv4 adress from instance details
def instance_ipv4(instance_details):
    for iface in instance_details['NetworkInterfaces']:
        if 'Association' in iface:
            if 'PublicIp' in iface['Association']:
                return iface['Association']['PublicIp']

# Get the IPv6 adress from instance details
def instance_ipv6(instance_details):
    for iface in instance_details['NetworkInterfaces']:
        if 'Ipv6Addresses' in iface:
            for ipv6 in iface['Ipv6Addresses']:
                if 'Ipv6Address' in ipv6:
                    return ipv6['Ipv6Address']

# Route53 change operation for batch updates
def route53_change(action, name, type, value):
    return {
        'Action': action,
        'ResourceRecordSet': {
            'Name': name,
            'Type': type,
            'TTL': 10,
            'ResourceRecords': [
                {
                    'Value': value
                }
            ]
        }
    }

# Update Route53 record sets
def change_recordsets(zone_id, hostname, ipv4, ipv6):
    ipv4_action = 'UPSERT' if ipv4 != None else 'DELETE'
    ipv6_action = 'UPSERT' if ipv6 != None else 'DELETE'

    client = boto3.client('route53')
    resp = client.change_resource_record_sets(
        HostedZoneId=zone_id,
        ChangeBatch={
            'Changes': [
                route53_change(ipv4_action, hostname, 'A', ipv4),
                route53_change(ipv6_action, hostname, 'AAAA', ipv6)
            ]
        }
    )
    return None
